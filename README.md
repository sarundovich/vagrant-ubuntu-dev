# README #

### Vagrant Ubuntu 12.04 x64 Precise development VM preconfigured with LAMP stack ###

This is a simple hashicorp/precise64 box with preconfigured
puppet provisioning scripts that loads/installs most common
LAMP tools and configures them for your comfotrable Web Development.

### Features ###

* Ubuntu 12.04 x64Precise
* git, curl, vim, mc system packages
* Apache2 on port 80 (guest machine) and 8080 on host machine with enabled mod_rewrite
* PHP 5.5
* xdebug on port 9000
* MySQL on port 3306 (guest machine) and 3307 on host machine. Root credentials: **vagrant_user/unsecurepassword** or **root/root**. db: **vagrant_db**
* PhpMyAdmin. The preferred way to connect to your database is using a dedicated application like [Sequel Pro (OS X)](http://www.sequelpro.com/), [HeidiSQL (Windows)](http://www.heidisql.com/), and [MySQL Workbench (Cross Platform)](http://dev.mysql.com/downloads/tools/workbench/) using SSH tunnel.
* Preconfigured dev vhost (for further details follow the next section) vagrant.dev:8080 and vagrant.dev:8080/phpmyadmin/
* SSH private key - /puphpet/files/dot/ssh/id_rsa (on your host machine)
* Provisioning provided by puphpet (https://puphpet.com/)


### How do I get set up? ###

First clone this repo then cd to the dir with a "Vagrantfile" and type "**vagrant up**". The process can take a while depending on your internet connection (Vagrant will try bo download OS image and up it with a provisioning). Then add to your "hosts" on the host machine following line:

```
#!bash

127.0.0.1  vagrant.dev www.vagrant.dev
```
You`re done! Now all scripts inside /var/www (guest machine) is available in your host machine file system - /{vagrant_dir}/htdocs. Also you can call scripts using url http://vagrant.dev:8080/

Now you can find shared folder with a doc roots for vhost following this path (on your host machine): /{vagrant_dir}/htdocs (It`s synced folder for /var/www dir on guest machine). For now you can start developing your projects as usual.

**!!! IMPORTANT !!!** If you can`t reach 8080 port (or 3307) on you host machine try to deal with your firewall. On some *NIX machines custom ports are disabled by iptables. To fix it just add ACCEPT rules for following ports: 8080 (http), 3307 (mysql). 
I.e.: 
```
#!bash

iptables -A INPUT -i eth0 -p tcp -m tcp --dport 8080 -j ACCEPT
```

Also you can ssh using your favorite client:
* username=vagrant
* private key=/puphpet/files/dot/ssh/id_rsa (on your host machine)

Or just "vagrant ssh" terminal command inside your VM dir (*NIX host OS only)

### Dependencies ###

* Git (Optional. You can download this repo as ZIP)
* VirtualBox 
* Vagrant >= 1.6

### Who do I talk to? ###

You can drop me lines on ragnar.indie@gmail.com. 
Also checkout my site: http://ragnarindie.info/